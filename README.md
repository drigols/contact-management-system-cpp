# Contact Management System (C++)

[![MIT License](https://img.shields.io/badge/license-MIT-007EC7.svg?style=flat-square)](LICENSE.md)

> A **Contact Management System** developed in *C++* to help you manage your contacts.

## Project Overview

 - [Credits](#credits)

```bash
g++ main.cpp cli.cpp contact.cpp -o Contact-Management-System.out && ./Contact-Management-System.out
```

---

<div div="credits"></div>

## Credits

This project was developed based on the ["C++ implementation of Phone Book Management System"](https://cppsecrets.com/users/22319897989712197103975756505164103109971051084699111109/C00-implementation-of-Phone-Book-Management-System.php) article.

---

Ro**drigo** **L**eite da **S**ilva
